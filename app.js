const cfg = require('./config.json');
const fs = require('fs');
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const serveStatic = require('serve-static');
const bodyParser = require('body-parser');
const Path = require('path');
const validator = require('./core/validator');

let server = null;

MongooseInit( (err) => {
    if( err ){
        ExitWithError( 'Mongoose Initialisation Error: ' + JSON.stringify(err) );
    }
    else {
        ExpressInit( (err, app) => {
            if( err ){
                ExitWithError( 'Express Initialisation Error: ' + JSON.stringify(err) );
            }
            else {
                server = app.listen( cfg.express.port, () => {
                    console.log( 'Server running on port ' + server.address().port );
                });
            }
        });
    }
});

function MongooseInit( callback ){
    if( !cfg.mongoose ){
        callback();
    }
    else {
        let un = cfg.mongoose.username;
        let pw = cfg.mongoose.password;
        let ht = cfg.mongoose.host;
        let pt = cfg.mongoose.port;
        let db = cfg.mongoose.database;
        
        mongoose.Promise = global.Promise;
        mongoose.connect( 'mongodb://'+un+':'+pw+'@'+ht+':'+pt+'/'+db );
        mongoose.connection.on( 'error', (err) => {
            console.log('Mongoose Error: ' + JSON.stringify(err));
        });
        
        let path = Path.join( __dirname, cfg.mongoose.models );
        fs.readdir( path, (err, files) => {
            if( err ){
                callback( err );
            }
            else {
                for( let i = 0; i < files.length; ++i ){
                    let filename = files[i];
                    let model = require( path + filename );
                    
                    filename = filename.substring( 0, filename.lastIndexOf('.') );
                    mongoose.model( filename, model );
                }
                
                callback();
            }
        });
    }
}

function ExpressInit( callback ){
    let app = express();
    let path = null;
    
    if( cfg.express && cfg.express.public ){
        path = Path.join( __dirname, cfg.express.public );
        app.use( serveStatic(path) );
    }

    path = Path.join( __dirname, cfg.express.routes );
    fs.readdir( path, (err, files) => {
        if( err ) {
            callback( err );
        }
        else {
            // Debug log the requests
            if( cfg.morgan ){
                app.use( morgan(cfg.morgan.formatter, cfg.morgan.options) );
            }

            // Override express' powered-by header for project flavour
            if( cfg.express.poweredBy ){
                app.use( (req, res, next) => {
                    res.set('x-powered-by', cfg.express.poweredBy );
                    next();
                });
            }
            
            // Make sure the client is valid
            if( cfg.express.clientAuth ){
                app.use( validator.ValidateClient );
            }

            app.use( bodyParser.urlencoded({ extended: false }) );
            app.use( bodyParser.json() );
            
            let openRoutes = [];
            let authRoutes = [];
            
            // Load up all routes
            for( let i = 0; i < files.length; ++i ){
                let route = require( Path.join(path, files[i]) );
                
                if( route.hasOwnProperty('public') ){
                    openRoutes.push( route.public );
                }
                
                if( route.hasOwnProperty('authenticated') ){
                    authRoutes.push( route.authenticated );
                }
            }
            
            // Set the routes up that do not need authentication
            for( let i = 0; i < openRoutes.length; ++i ){
                app.use( '/api', openRoutes[i] );
            }
            
            // Ensure the user is authenticated
            if( cfg.express.userAuth ){
                app.use( validator.ValidateUser );
            }
            
            // Set the routes up that require authentication
            for( let i = 0; i < authRoutes.length; ++i ){
                app.use( '/api', authRoutes[i] );
            }
            
            // Handle any other requests
            app.use( (req, res) => {
                res.sendStatus( 404 );
            });
            
            callback( null, app );
        }
    });
}

function ExitWithError( err ){
    console.log( err );
    process.exit( 1 );
}