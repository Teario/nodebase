var mongoose = require( 'mongoose' );
var bcrypt = require( 'bcrypt' );
var express = require( 'express' );
var crypto = require( 'crypto' );
var router = express.Router();

function generateAccessToken( userId, callback ){
  if( callback ){
    crypto.randomBytes( 32, function( err, bytes ) {
      if( err || !bytes ){
        callback( err, null );
      }
      else {
        var hmac = crypto.createHmac( 'sha256', userId.toString() );
        hmac.setEncoding( 'hex' );
        hmac.write( bytes );
        hmac.end();
        
        var token = hmac.read();
        
        var AccessToken = mongoose.model( 'accesstoken' );
        var accessToken = new AccessToken({
          'owner' : userId,
          'token' : token
        });
        
        accessToken.save( function( err ) {
          if( err ){
            callback( err, null );
          }
          else {
            callback( null, accessToken );
          }
        });
      }
    });
  }
}

router.post( '/user', function( req, res, next ) {
  var username = req.body.username;
  var email = req.body.email;
  var password = req.body.password;
  
  console.log(JSON.stringify(req.body));
  
  if( !username ) {
    res.json({ 'field' : 'username', 'reason' : 'missing' }, 400 );
  } else if( !email ) {
    res.json({ 'field' : 'email', 'reason' : 'missing' }, 400 );
  } else if( !password ) {
    res.json({ 'field' : 'password', 'reason' : 'missing' }, 400 );
  } else {
    bcrypt.genSalt( 11, function( err, salt ) {
      if( err || !salt ){
        res.sendStatus( 500 );
      }
      else {
        bcrypt.hash( password, salt, function( err, hash ) {
          if( err || !hash ){
            res.sendStatus( 500 );
          }
          else {
            var UserModel = mongoose.model( 'user' );
            var user = new UserModel({
              'username' : username,
              'email' : email,
              'password' : hash
            });
            
            user.save( function( err, newUser ) {
              if( err ) {
                res.sendStatus( 500 );
              }
              else {
                generateAccessToken( newUser._id, function( err, accessToken ){
                  if( err ){
                    res.sendStatus( 500 );
                  }
                  else {
                    res.send({
                      'user' : user,
                      'token' : accessToken.token
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
});

router.post( '/user/auth', function( req, res, next ) {
  var username = req.body.username;
  var password = req.body.password;
  
  if( !username ) {
    res.json({ 'field' : 'username', 'reason' : 'missing' }, 400 );
  }
  else if( !password ) {
    res.json({ 'field' : 'password', 'reason' : 'missing' }, 400 );
  }
  else {
    var UserModel = mongoose.model( 'user' );
    UserModel.findOne( { 'username' : username }, 'username email password', function( err, user ){
      if( err || !user ){
        res.sendStatus( 403 );
      }
      else {
        bcrypt.compare( password, user.password, function( err, match ) {
          if( err || !match ){
            res.sendStatus( 403 );
          }
          else {
            // Generate an access token...
            crypto.randomBytes( 32, function( err, bytes ) {
              if( err || !bytes ){
                res.sendStatus( 500 );
              }
              else {
                generateAccessToken( user._id, function( err, accessToken ){
                  if( err ){
                    res.sendStatus( 500 );
                  }
                  else {
                    res.send({
                      'user' : user,
                      'token' : accessToken.token
                    });
                  }
                });
              }
          });
        }
      });
      }
    });
  }
});

module.exports = {
  'public' : router
};