function Authenticator(){
    var that = this;
    this.user = null;

    this.isAuthenticated = function(){
        return that.user !== null && false;
    };
    
    $( document ).ajaxSend( function( event, xhr, options ){
        xhr.setRequestHeader( 'x-mercury-token', 'yT478uSQg91bIGG7kB45zk7u8Q83jQJu639BUQ98R34IYxib8v0237qQj15HLo3Q' );

        if( that.user && that.user.token ){
            xhr.setRequestHeader( 'x-user-token', that.user.token );
        }
    } );

    $( document ).ajaxError( function( event, xhr, error ){
        that.user = null;
    } );

    this.login = function( email, password ){
        that.sendUserRequest( '/api/user/auth', email, password );
    };

    this.register = function( email, password ){
        that.sendUserRequest( '/api/user', email, password );
    };

    this.sendUserRequest = function( endpoint, email, password ){
        if( that.user ){
            that.user = null;
        }

        $.ajax(endpoint, {
            'method': 'POST',
            'data': JSON.stringify({
                'email': email,
                'password': password
            }),
            'contentType': 'application/json; charset=utf-8',
            'success': that.onUserArrived,
            'error': that.onUserFailed
        });
    };

    this.onUserArrived = function( data, status, xhr ){
        console.log('User ',data);

        that.user = data.user;
        that.user.token = data.token;

        store.set( 'user', that.user );
    };

    this.onUserFailed = function( xhr, status, error ){
        console.log('USER FAIL');
    };
}
$( function(){
    window.authenticator = new Authenticator();
    window.authenticator.login('teario@gmail.com','testpassword');
} );