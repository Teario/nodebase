var mongoose = require( 'mongoose' );
var ObjectId = mongoose.Schema.ObjectId;

var AccessToken = new mongoose.Schema({
	owner: { type: ObjectId, required: true },
	token: { type: String, required: true, unique: true },
	createdAt: { type: Date, expires: '7d', default: Date.now }
});

AccessToken.set( 'toJSON', {
    transform: function( doc, ret ){
        var data = {
            value: ret.token
        };
		
        return data;
    }
});

module.exports = AccessToken;