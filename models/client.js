var mongoose = require( 'mongoose' );

var Client = new mongoose.Schema({
	name: { type: String, required: true, unique: true },
	token: { type: String, required: true, unique: true }
});

module.exports = Client;