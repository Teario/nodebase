var mongoose = require( 'mongoose' );

var User = new mongoose.Schema({
	email: { type: String, required: true, unique: true },
	username: { type: String, required: true, unique: true },
	password: { type: String, required: true, select: false }
});

User.set( 'toJSON', {
    transform: function( doc, ret ){
    	// Not all fields should be returned
        var data = {
            username: ret.username,
            email: ret.email,
            _id: ret._id
        };
        return data;
    }
});

module.exports = User;