const mongoose = require('mongoose');

module.exports.ValidateClient = function( req, res, next ) {
    if (!req.headers.hasOwnProperty('x-PROJECT-token')) {
        res.sendStatus(403);
    }
    else {
        let token = req.headers['x-PROJECT-token'];

        let Client = mongoose.model('client');
        Client.findOne({ 'token': token }, (err, client) => {
            if (err || !client) {
                res.sendStatus(403);
            }
            else {
                next();
            }
        });
    }
}

module.exports.ValidateUser = function( req, res, next ) {
    if (!req.headers.hasOwnProperty('x-user-token')) {
        res.sendStatus(403);
    }
    else {
        let token = req.headers['x-user-token'];

        const AccessToken = mongoose.model('AccessToken');
        AccessToken.findOne({ 'token': token }, (err, accessToken) => {
            if (err || !accessToken) {
                res.sendStatus(403);
            }
            else {
                const UserModel = mongoose.model('User');
                UserModel.findOne({ '_id': accessToken.owner }, (err, user) => {
                    if (err || !user) {
                        res.sendStatus(403);
                    }
                    else {
                        req.models = {
                            'user': user
                        };
                        next();
                    }
                });
            }
        });
    }
}